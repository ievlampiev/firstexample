/**
 * Created by Ilya Evlampiev on 10.09.2018.
 */
public class Example2 {
    //comment example2
    public static void main(String[] args) {
        System.out.println("Hello world2");

        StringBuilder sb1=new StringBuilder("One");
        StringBuilder sb2=sb1;
        System.out.println(sb1);
        System.out.println(sb2);
        sb1.setCharAt(0, 'T');
        sb1.setCharAt(1, 'w');
        sb1.setCharAt(2,'o');
        System.out.println(sb1);
        System.out.println(sb2);

        System.out.println("----");

        sb1=new StringBuilder("One");
        sb2=sb1;
        System.out.println(sb1);
        System.out.println(sb2);
        sb1=new StringBuilder("Two");
        System.out.println(sb1);
        System.out.println(sb2);

        System.out.println("----");

        String s1="One";
        String s2=s1;
        System.out.println(s1);
        System.out.println(s2);
        s1="Two";
        System.out.println(s1);
        System.out.println(s2);
    }
}
