package binary.search;

/**
 * Created by Ilya Evlampiev on 08.10.2018.
 */
public class BinarySearch {

    private double[] array;

    BinarySearch(double[] array) {
        this.array = array;
    }

    public int search(double toCompare) {
        if (array == null) {
            return -1;
        }
        int max = array.length - 1;
        int min = 0;
        int index = -1;
        int mid;
        while (min <= max) {
            mid = (max + min) / 2;
            if (toCompare > array[mid]) {
                min = mid + 1;
            } else if (toCompare < array[mid]) {
                max = mid - 1;

            } else {
                index = mid;
                break;
            }
        }


        //System.out.println(this.array);
        return index;
    }

    public static void main(String[] args) {
//        BinarySearch bs1=new BinarySearch(new double[]{0,1});
//        System.out.println("Search for -2 :" + bs1.search(-2));
//        System.out.println("Search for 0 :" + bs1.search(0));
//        System.out.println("Search for 1 :" + bs1.search(1));

        BinarySearch bs2 = new BinarySearch(new double[]{-1, 1, 2, 3, 45, 67});
        System.out.println("Search for -3 :" + bs2.search(-3));
        System.out.println("Search for -1 :" + bs2.search(-1));
        System.out.println("Search for 1 :" + bs2.search(1));
        System.out.println("Search for 2 :" + bs2.search(2));
        System.out.println("Search for 3 :" + bs2.search(3));
        System.out.println("Search for 45 :" + bs2.search(45));
        System.out.println("Search for 67 :" + bs2.search(67));

        System.out.println("---");

        BinarySearch bs3 = new BinarySearch(new double[]{-1, 1, 2, 3, 34, 45, 67});
        System.out.println("Search for -3 :" + bs3.search(-3));
        System.out.println("Search for -1 :" + bs3.search(-1));
        System.out.println("Search for 1 :" + bs3.search(1));
        System.out.println("Search for 2 :" + bs3.search(2));
        System.out.println("Search for 3 :" + bs3.search(3));
        System.out.println("Search for 4 :" + bs3.search(4));
        System.out.println("Search for 34 :" + bs3.search(34));
        System.out.println("Search for 45 :" + bs3.search(45));
        System.out.println("Search for 67 :" + bs3.search(67));
        System.out.println("Search for -1 :" + bs3.search(-1));

//        BinarySearch bs4=new BinarySearch(new double[]{0});
//        System.out.println("Search for -2 :" + bs4.search(-2));
//        System.out.println("Search for 0 :" + bs4.search(0));
//        System.out.println("Search for 1 :" + bs4.search(1));
//
//        BinarySearch bs5=new BinarySearch(new double[]{});
//        System.out.println("Search for -2 :" + bs5.search(-2));
//        System.out.println("Search for 0 :" + bs5.search(0));
//        System.out.println("Search for 1 :" + bs5.search(1));
//
//        BinarySearch bs6=new BinarySearch(null);
//        System.out.println("Search for -2 :" + bs6.search(-2));
//        System.out.println("Search for 0 :" + bs6.search(0));
//        System.out.println("Search for 1 :" + bs6.search(1));
//
    }

}
