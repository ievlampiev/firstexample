package exceptions;

import com.sun.org.apache.xpath.internal.SourceTree;
import itis.oop.Student;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Created by Ilya Evlampiev on 14.11.2018.
 */
public class ExceptionTest {

    public static void main(String[] args) {
        try {
            called(args);
        } catch (FileNotFoundException|WrongPodzorException e) {
            //e.printStackTrace();
            System.out.println(e.getMessage());
            System.out.println("Перехват исключения во внешнем методе");
        }
        finally {
            System.out.println("Anyway");
        }
    }

    public static void called(String[] args) throws FileNotFoundException, WrongPodzorException {
        FileInputStream fileInputStream = null;
        boolean fileExist=false;
        while(!fileExist) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
                fileInputStream = new FileInputStream("file2");

            try{
                System.out.println(fileInputStream.read());
                fileExist = true;
            }
            catch (NullPointerException|IOException e) {
                e.printStackTrace();
                System.out.println(e.getLocalizedMessage());
                System.out.println("Какая-то ошибка произошла");
            }

            //throw new WrongPodzorException("Wrong Podzor");
            //throw new FileNotFoundException("");
            //throw new IOException("");
            //throw new NullPointerException("Выкидываем внутри");

        }
        System.out.println("Теперь перейдем к действиям с файлом");
    }
}
