package exceptions;

/**
 * Created by Ilya Evlampiev on 14.11.2018.
 */
public class WrongPodzorException extends Exception {
    WrongPodzorException(String message)
    {
        super(message);
    }

}
