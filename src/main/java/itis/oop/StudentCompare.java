package itis.oop;

import java.util.Date;

/**
 * Created by Ilya Evlampiev on 07.11.2018.
 */
public class StudentCompare {
    public static void main(String[] args) {
        Date newDate=new Date();
        Student student0=new Student(newDate);
        student0.setFirstName("StudentName0");
        student0.setPatronymic("StudentPatronymic0");
        student0.setSecondName("StudentSecondName0");
        Student student1=new Student(newDate);
        student1.setFirstName("StudentName0");
        student1.setPatronymic("StudentPatronymic0");
        student1.setSecondName("StudentSecondName0");
        //student0=student1;

        if (student0 == student1)
        {
            System.out.println(student0.toString()+"=="+student1.toString());
        }
        else
        {
            System.out.println(student0.toString()+"!="+student1.toString());
        }

        if (student0.equals(student1))
        {
            System.out.println(student0.toString()+" .equals() "+student1.toString());
        }
        else
        {
            System.out.println(student0.toString()+" not .equals() "+student1.toString());
        }

        System.out.println(student0.hashCode());
        System.out.println(student1.hashCode());
        System.out.println(student0.getClass().toString());
        System.out.println(student1.getClass().toString());


    }
}
