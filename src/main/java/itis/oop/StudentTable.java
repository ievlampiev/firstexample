package itis.oop;

import java.util.Date;

/**
 * Created by Ilya Evlampiev on 03.10.2018.
 */
public class StudentTable {
    public static void main(String[] args) {
//        int groupSize=100;
//        String[] firstNames=new String[groupSize];
//        String[] secondNames=new String[groupSize];
//        String[] patronymics=new String[groupSize];
//        String[] dateOfBirth=new String[groupSize];


        StudentGroup group11_805=new StudentGroup(25,"11-805");

        for (int i=0; i<group11_805.groupSize; i++)
        {

            Date studentDateOfBirth=new Date();
            studentDateOfBirth.setYear(90+(int)(Math.random()*10));
            studentDateOfBirth.setMonth(6);
            studentDateOfBirth.setDate(15);
            Student student=new Student(studentDateOfBirth);
            student.dateOfBirth=studentDateOfBirth;
            student.firstName="Artem";
            student.secondName="Samigullin";
            student.patronymic="Ildarovich";
            student.email="email"+i+"@mail.ru";
            //student.bitbucketRepo="https://bitbucket.org/Enoxus/";
            group11_805.students[i]=student;
        };

        Date studentDateOfBirth=new Date();
        studentDateOfBirth.setYear(100);
        studentDateOfBirth.setMonth(6);
        studentDateOfBirth.setDate(15);
        Student student1=new Student(studentDateOfBirth);

        student1.dateOfBirth=studentDateOfBirth;
        student1.firstName="Artem";
        student1.secondName="Samigullin";
        student1.patronymic="Ildarovich";
        student1.email="email@mail.ru";
        //student1.bitbucketRepo="https://bitbucket.org/Enoxus/";
        System.out.println(student1.age());
        Date studentDateOfBirth2=new Date();
        studentDateOfBirth2.setYear(100);
        studentDateOfBirth2.setMonth(6);
        studentDateOfBirth2.setDate(15);
        Student student2=new ITISStudent(studentDateOfBirth);
        //student2.dateOfBirth=studentDateOfBirth2;
        student2.firstName="Ruslan";
        student2.secondName="Gataullin";
        student2.patronymic="Timurovich";
        student2.email="email2@mail.ru";
        ((ITISStudent)student2).bitbucketRepo="https://bitbucket.org/slvtk/";
        System.out.println(student2.age());

        for (Student student: group11_805.students)
        {
            System.out.println(student.email+" "+student.age());
        }
        System.out.println();
        System.out.println(student1.age());
        System.out.println(student2.age());
        student1.dateOfBirth.setYear(101);
        System.out.println(student1.age());
        System.out.println(student2.age());
        System.out.println();
    }
}
