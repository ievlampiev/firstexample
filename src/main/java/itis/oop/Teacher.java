package itis.oop;

/**
 * Created by Ilya Evlampiev on 22.10.2018.
 */
public interface Teacher {

    public void mark(Student student);

    public void setName (String name);

    public void setSubject (String subject);


}
