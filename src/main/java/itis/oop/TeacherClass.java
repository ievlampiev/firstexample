package itis.oop;

/**
 * Created by Ilya Evlampiev on 24.10.2018.
 */
public abstract class TeacherClass  {
    abstract public void mark(Student student);

    private String name;
    private String subject;

    public String getName() {
        return name;
    }

    public String getSubject() {
        return subject;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }
}
