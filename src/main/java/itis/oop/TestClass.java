package itis.oop;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Ilya Evlampiev on 17.10.2018.
 */
public class TestClass {
    public static void  main (String[] args) {
        Student st1 = new Student(new Date());
        st1.firstName = "Pavel";
        st1.secondName = "Ivanov";
        st1.patronymic = "Alexeevich";
        st1.setYear(1999);
        TeacherClass teacher1 = new UnjustTeacher();
        TeacherClass teacher2 = new JustTeacher();
        TeacherClass[] teachers1 = new TeacherClass[2];

        teacher2.setName("Egor");
        teacher2.setSubject("IT");
        teacher1.setName("Igor");
        teacher1.setSubject("math");

        Student st2 = new Student(new Date());
        st2.firstName = "Ruslan";
        st2.secondName = "Ivanov";
        st2.patronymic = "Igorevich";
        st2.setYear(2001);
        teachers1[0] = teacher1;
        teachers1[1] = teacher2;

        for (TeacherClass teacher : teachers1) {
            teacher.mark(st1);
            teacher.mark(st2);
        }
    }


}
