package itis.oop;

/**
 * Created by Ilya Evlampiev on 22.10.2018.
 */
public class UnjustTeacher extends TeacherClass {

    public void mark(Student student) {

        int k = (int) (Math.random()*3)+2;
        if (student.getYear()<2000){
            if (k!=2){
                k--;

            }
        }
        String m = "";
        if (k==2) {
            m = "neud";
        }
        else if (k==3) {
            m="ud";
        }
        else if (k==4){
            m="hor";
        }
        else if (k==5) {
            m="otl";
        }
        System.out.println("Prepod " + getName() + " ocenil studenta " + student.getFirstName() + " " + student.getSecondName() + " " + student.getPatronymic() + " po predmetu  " + getSubject() + " " + " na ocenku " + m);
    }

}
