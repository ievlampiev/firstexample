package sort.bubble;

import java.util.Arrays;

/**
 * Created by Ilya Evlampiev on 10.10.2018.
 */
public class BubbleSort {
    private double[] array;
    int swapCounter =0;
    int runsCounter =0;

    BubbleSort(double[] array)
    {
        this.array=array;
    }

    void print()
    {
        System.out.println(Arrays.toString(this.array));
    }
    void printSwapCounter(){
        System.out.println(swapCounter);
    }

    void printRunsCounter(){
        System.out.println(runsCounter);
    }

    void sort()
    {
        double temp;
        boolean wasSwap;
        int n = array.length;
        while(n != 0){
            runsCounter++;
            wasSwap=false;
            for(int i = 1; i < n; i++){
                if(array[i]< array[i-1]){
                    temp = array[i];
                    array[i] = array[i-1];
                    array[i-1] = temp;
                    swapCounter++;
                    wasSwap=true;
                }
            }
            if (!wasSwap) {break;}
            n--;
        }

        //this.array;
    }

    public static void main(String[] args) {
        BubbleSort bubbleSort1=new BubbleSort(new double[]{1, -1, 56, 3, 100, 67});
        bubbleSort1.print();
        bubbleSort1.sort();
        bubbleSort1.print();
        bubbleSort1.printSwapCounter();
        bubbleSort1.printRunsCounter();

        BubbleSort bubbleSort2=new BubbleSort(new double[]{1, 2, 5, 23, 100, 167});
        bubbleSort2.print();
        bubbleSort2.sort();
        bubbleSort2.print();
        bubbleSort2.printSwapCounter();
        bubbleSort2.printRunsCounter();

        BubbleSort bubbleSort3 = new BubbleSort(new double[]{3, 2, 7, 5, 3, 8});
        bubbleSort3.print();
        bubbleSort3.sort();
        bubbleSort3.print();
        bubbleSort3.printSwapCounter();
        bubbleSort3.printRunsCounter();

        BubbleSort bubbleSort4 = new BubbleSort(new double[]{30, 20, 7, 5, 3, 1});
        bubbleSort4.print();
        bubbleSort4.sort();
        bubbleSort4.print();
        bubbleSort4.printSwapCounter();
        bubbleSort4.printRunsCounter();

    }

}
