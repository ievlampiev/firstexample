package sort.quicksort;

import java.util.Arrays;

/**
 * Created by Ilya Evlampiev on 15.10.2018.
 */
public class QuickSort {

    static double[] array;

    static void print()
    {
        System.out.println(
      Arrays.toString(array));
    }

    static void input(double[] arrayInput)
    {
        array=arrayInput;
    }
    static void quickSort(int min, int max)
    {
        double pivot = array[(min + max)/ 2];
        int pivotIndex = (min + max) / 2;
        double temp;

        if(min >= max){
            return;
        }

        int i = min;
        int j = max;

        while(i < j){
            while(i < pivotIndex && array[i] <= pivot){
                i++;


            }
            while(j > pivotIndex && array[j] >= pivot){
                j--;
            }
            if(i < j){
                temp = array[i];
                array[i] = array[j];
                array[j] = temp;
                if(i == pivotIndex){
                    pivotIndex = j;
                }else if(j == pivotIndex){
                    pivotIndex = i;
                }
            }
            }

        quickSort(min, pivotIndex);
        quickSort(pivotIndex + 1, max);
    };

    public static void main(String[] args) {
//        double[] array=new double[] {2,15,13,10,5,9,20};
//        QuickSort.input(array);
//        QuickSort.print();
//        QuickSort.quickSort(0,array.length-1);
//        QuickSort.print();
//
//        System.out.println("---");
//
//        double[] array2=new double[] {2,15,13,10,10,5,9,20};
//        QuickSort.input(array2);
//        QuickSort.print();
//        QuickSort.quickSort(0,array2.length-1);
//        QuickSort.print();
//
//        System.out.println("---");
//
//        double[] array3=new double[] {2,13,13,10,5,0,20};
//        QuickSort.input(array3);
//        QuickSort.print();
//        QuickSort.quickSort(0,array3.length-1);
//        QuickSort.print();
//
//        System.out.println("---");

        double[] array4=new double[] {9,8,7,6,7,8,9};
        QuickSort.input(array4);
        QuickSort.print();
        QuickSort.quickSort(0,array4.length-1);
        QuickSort.print();

        System.out.println("---");

        double[] array5=new double[] {6,7,8,9,7,8,6};
        QuickSort.input(array5);
        QuickSort.print();
        QuickSort.quickSort(0,array5.length-1);
        QuickSort.print();

        System.out.println("---");

        double[] array6=new double[] {6,7,8,9,10,11,12};
        QuickSort.input(array6);
        QuickSort.print();
        QuickSort.quickSort(0,array6.length-1);
        QuickSort.print();

        System.out.println("---");

        double[] array7=new double[] {6,7,8,9,10,11,12,13};
        QuickSort.input(array7);
        QuickSort.print();
        QuickSort.quickSort(0,array7.length-1);
        QuickSort.print();

        System.out.println("---");

        double[] array8=new double[] {12,11,10,9,8,7,6};
        QuickSort.input(array8);
        QuickSort.print();
        QuickSort.quickSort(0,array8.length-1);
        QuickSort.print();

        System.out.println("---");

        double[] array9=new double[] {13,12,11,10,9,8,7,6};
        QuickSort.input(array9);
        QuickSort.print();
        QuickSort.quickSort(0,array.length-1);
        QuickSort.print();

        System.out.println("---");

        double[] array10=new double[] {1,2,3,4,0,5,6,7,8};
        QuickSort.input(array10);
        QuickSort.print();
        QuickSort.quickSort(0,array.length-1);
        QuickSort.print();

    }
}
