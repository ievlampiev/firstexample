package univer;

/**
 * Created by Ilya Evlampiev on 28.11.2018.
 */
public abstract class AbstractTeacherFabric {
    public Teacher createTeacher(String parameter, String name)
    {
        TeacherSimpleFabric teacherSimpleFabric=new TeacherSimpleFabric();
        Teacher teacher=teacherSimpleFabric.createTeacher(parameter,name);
        teacher.setSubject(createSubject());
        teacher.setHobby(createHobby());
        teacher.setSalary(createSalary());
        return teacher;
    };

    public abstract ISubject createSubject();

    public abstract IHobby createHobby();

    public abstract int createSalary();
}
