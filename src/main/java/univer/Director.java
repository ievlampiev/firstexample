package univer;

/**
 * Created by Ilya Evlampiev on 26.12.2018.
 */
public class Director implements ISign {
    private String name;

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void setName(String name) {
       this.name=name;
    }
}
