package univer;

import java.io.*;
import java.util.ArrayList;
import java.util.stream.Collectors;

/**
 * Created by Ilya Evlampiev on 21.11.2018.
 */
public class FileUtil {

    private boolean hasWeOpenedFile = false;

    public Student[] readStudentsFromFile(String filePath) throws IOException {
        if (!hasWeOpenedFile)
        {
            hasWeOpenedFile=true;
            BufferedReader reader = new BufferedReader(new FileReader(filePath));
            //read all students
            ArrayList<String> studentslist = reader.lines().collect(Collectors.toCollection(ArrayList::new));
            ArrayList<String> seenStudents = new ArrayList<>();
            Student[] students = new Student[studentslist.size()];
            int i = 0;
            for (String student : studentslist) {
                String[] content = student.split(":");
                if (!seenStudents.contains(content[0])) {
                    seenStudents.add(content[0]);
                } else {
                    throw new NotUniqueNameException("Имена студентов не уникальны");

                }
                students[i] = new Student(content[0], Integer.parseInt(content[1]));
                i++;
            }
            PrintWriter writer = new PrintWriter(filePath);
            writer.print("");
            writer.close();
            reader.close();
            // delete all rows from file
            return students;
        }
        else
        {
            return null;
        }
    }

    public void writeStudentsToFile(String filePath, Student[] students) throws IOException {
        // write Student[] students to file
        FileOutputStream writer = new FileOutputStream(filePath);
        StringBuilder content = new StringBuilder();
        for (Student student : students) {
            content.append(student.getName() + ":" + student.getTotalGrade());
            content.append("\n");
        }
        writer.write(content.toString().getBytes());
        writer.close();
    }

    public Teacher[] readTeachersFromFile(String filePath) throws IOException {
        if (!hasWeOpenedFile)
        {
            hasWeOpenedFile=true;
            BufferedReader reader = new BufferedReader(new FileReader(filePath));
            //read all students
            ArrayList<String> studentslist = reader.lines().collect(Collectors.toCollection(ArrayList::new));
            ArrayList<String> seenStudents = new ArrayList<>();
            Teacher[] teachers = new Teacher[studentslist.size()];
            int i = 0;
            for (String teacher : studentslist) {
                String[] content = teacher.split(":");
                if (!seenStudents.contains(content[0])) {
                    seenStudents.add(content[0]);
                } else {
                    throw new NotUniqueNameException("Имена студентов не уникальны");

                }
                TeacherSimpleFabric teacherSimpleFabric=new TeacherSimpleFabric();
                teachers[i]=teacherSimpleFabric.createTeacher(content[1],content[0]);
                    i++;

                }
            PrintWriter writer = new PrintWriter(filePath);
            writer.print("");
            writer.close();
            reader.close();
            // delete all rows from file
            return teachers;
        }
        else
        {
            return null;
        }
    }

    public void writeTeachersToFile(String filePath, Teacher[] teachers) throws IOException {
        // write Student[] students to file
        FileOutputStream writer = new FileOutputStream(filePath);
        StringBuilder content = new StringBuilder();
        for (Teacher teacher : teachers) {
            String digit ;
            if (teacher instanceof TeacherCampus) {
                digit = "0";
            }
            else {
                digit = "1";
            }
            content.append(teacher.getName() + ":" + digit);
            content.append("\n");
        }
        writer.write(content.toString().getBytes());
        writer.close();
    }

}
