package univer;

/**
 * Created by Ilya Evlampiev on 19.11.2018.
 */
public interface IHaveAccess extends ISign{

    public void takeKey();
}
