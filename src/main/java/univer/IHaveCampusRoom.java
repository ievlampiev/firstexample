package univer;

/**
 * Created by Ilya Evlampiev on 19.11.2018.
 */
public interface IHaveCampusRoom {
    public boolean fireSafetyChecking();

    public String getSign();
}
