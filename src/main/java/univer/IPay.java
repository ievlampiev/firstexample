package univer;

/**
 * Created by Ilya Evlampiev on 19.11.2018.
 */
public interface IPay {
    public void payMoney(int money);

    public String getSign();
}
