package univer;

/**
 * Created by Ilya Evlampiev on 19.11.2018.
 */
public interface ISign {
    default public String getSign()
    {
        return "Подпись "+getName();
    }

    public String getName();

    public void setName(String name);
}
