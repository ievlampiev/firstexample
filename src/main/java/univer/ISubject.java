package univer;

/**
 * Created by Ilya Evlampiev on 28.11.2018.
 */
public interface ISubject {
    public void teach();
}
