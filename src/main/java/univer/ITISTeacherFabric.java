package univer;

/**
 * Created by Ilya Evlampiev on 28.11.2018.
 */
public class ITISTeacherFabric extends AbstractTeacherFabric {

    @Override
    public ISubject createSubject() {
        return Programming.getInstance();
    }

    @Override
    public IHobby createHobby() {
        return new BrainRing();
    }

    @Override
    public int createSalary() {
        return 100;
    }
}
