package univer;

/**
 * Created by Ilya Evlampiev on 21.11.2018.
 */
public class NotUniqueNameException extends IllegalArgumentException {
    public NotUniqueNameException(String s) {
        super(s);
    }
}
