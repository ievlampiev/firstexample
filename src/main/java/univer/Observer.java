package univer;

/**
 * Created by Ilya Evlampiev on 05.12.2018.
 */
public interface Observer {
    public void update();
    public void doAfterUpdate();
    public void setObservable(Subject subject);
}
