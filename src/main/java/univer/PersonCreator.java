package univer;

/**
 * Created by Ilya Evlampiev on 26.11.2018.
 */
public abstract class PersonCreator {

    protected abstract ISign create(String typeParameter, String name);

    public ISign createObjectAndLogSign(String typeParameter, String name)
    {
        ISign iSign=create(typeParameter, name);
        System.out.println("New object is creatd, getting it's sign "+iSign.getSign());
        return iSign;
    }


}
