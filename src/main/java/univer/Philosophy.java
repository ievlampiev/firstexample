package univer;

/**
 * Created by Ilya Evlampiev on 28.11.2018.
 */
public class Philosophy implements ISubject {
    private static Philosophy instance;

    public static Philosophy getInstance() {
        if (instance!=null) {
            return instance;
        }
        else {
            instance = new Philosophy();
            return instance;
        }
    }
    private Philosophy() {

    }
    @Override
    public void teach() {
        System.out.println("Вспоминает имперавтив Канта");
    }
}
