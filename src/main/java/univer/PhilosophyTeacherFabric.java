package univer;

/**
 * Created by Ilya Evlampiev on 28.11.2018.
 */
public class PhilosophyTeacherFabric extends AbstractTeacherFabric {


    @Override
    public ISubject createSubject() {
        return Philosophy.getInstance();
    }

    @Override
    public IHobby createHobby() {
        return new Chorus();
    }

    @Override
    public int createSalary() {
        return 99;
    }
}
