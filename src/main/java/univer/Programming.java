package univer;

/**
 * Created by Ilya Evlampiev on 28.11.2018.
 */
public class Programming implements ISubject {
    private static Programming instance;

    public static Programming getInstance() {
        if (instance != null) {
            return instance;
        }
        else {
            instance = new Programming();
            return instance;
        }

    }
    private Programming() {

    }
    @Override
    public void teach() {
        System.out.println("Начинает пару по программированию");
        System.out.println("Вводит sout");
        System.out.println("Написал в коде System.out.println...");
    }
}
