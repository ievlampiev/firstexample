package univer;

/**
 * Created by Ilya Evlampiev on 24.12.2018.
 */
public class Room<T, S> {
    private T liver;
    private S liver2;
    private int number;

    public T getLiver() {
        return liver;
    }

    public void setLiver(T liver) {
        this.liver = liver;
    }

    public S getLiver2() {
        return liver2;
    }

    public void setLiver2(S liver2) {
        this.liver2 = liver2;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }
}
