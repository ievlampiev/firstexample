package univer;

import univer.canteen.Dish;

/** class for studying
 * Created by Ilya Evlampiev on 19.11.2018.
 */
public class Student implements IHaveAccess, IHaveCampusRoom, Observer {
    private String name;
    private int totalGrade;

    private Subject observable;

    public void setObservable(Subject observable) {
        this.observable = observable;
    }

    public Subject getObservable() {
        return observable;
    }
    public int getTotalGrade() {
        return totalGrade;
    }

    private int grade;

    public Student(String name, Teacher teacher) {
        this.name = name;
        //this.teacher = teacher;
    }

    @Override
    public void takeKey() {
        System.out.println("Студент " + name + " берет ключ");
    }

    @Override
    public boolean fireSafetyChecking() {
        return false;
    }

    /**
     *
     * @return Students Personal Sign
     *
     */

    @Override
    public String getSign() {
        //return "(подпись студента) " + name + "/учитель "+teacher.getSign()+"/";
        return "(подпись студента) " + name;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
        this.totalGrade=this.totalGrade+grade;
    }

    /**
     * @param name
     * Students Personal Name
     *
     * @param totalGrade
     * Students Exam Grade from all homeworks
     */


    public Student(String name, int totalGrade) {
        this.name = name;
        this.totalGrade = totalGrade;
    }

    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name=name;
    }

    public Student(String name){
        this.name=name;
    }



    @Override
    public void update() {
        doAfterUpdate();
    }


    /** in this method student doing his homework
     * obtaned from observable teacher
     *
     */
    @Override

    public void doAfterUpdate() {
        System.out.println(name+" делает домашнюю работу с содержанием: "+observable.currentHomework());
    }

    public void eatLunch(Dish dish)
    {
        System.out.println(name+" ест блюдо: "+dish.getName()+", заплатив за него "+dish.getCost());
    }
}

