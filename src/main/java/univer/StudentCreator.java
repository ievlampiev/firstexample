package univer;

/**
 * Created by Ilya Evlampiev on 26.11.2018.
 */
public class StudentCreator  extends PersonCreator{
    @Override
    protected ISign create(String typeParameter, String name) {
        return new Student(name);
    }
}
