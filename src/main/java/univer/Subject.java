package univer;


/**
 * Created by Ilya Evlampiev on 05.12.2018.
 */
public interface Subject {
    public void addObserver(Observer observer);
    public void removeObserver(Observer observer);
    public void notifyObservers();
    public String currentHomework();
}
