package univer;

import java.util.ArrayList;
import java.util.List;

/** class for work with entity of teacher
 *  class for doing exercises with {@link univer.Student}
 * Created by Ilya Evlampiev on 19.11.2018.
 */


abstract public class Teacher implements IHaveAccess, IGetPaid, Subject,Observer{
    private List<Observer> observers=new  ArrayList<Observer>();

    private String currentHomework;
    private Subject observable;

    public void setObservable(Subject observable) {
        this.observable = observable;
    }

    public Subject getObservable() {
        return observable;
    }

    public String getCurrentHomework() {
        return currentHomework;
    }

    public void setCurrentHomework(String currentHomework) {
        this.currentHomework = currentHomework;
    }

    private String name;

    private ISubject subject;

    private IHobby hobby;

    private int salary;

    public void setHobby(IHobby hobby) {
        this.hobby = hobby;
    }

    public Teacher(String name)
    {
        this.name=name;
    }

    @Override
    public void takeKey() {
        System.out.println("Учитель " + name + " берет ключ");
    }

    @Override
    public String getSign() {
        return "(подпись учителя) " + name;
    }

    @Override
    public void takeMoney(int count)
    {
        System.out.println("Преподаватель получил зп "+count);
    }

    abstract String whereLive();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void teach()
    {
        //System.out.println("Преподаватель провел пару по информатике");
        subject.teach();
    }

    public void setSubject(ISubject subject) {
        this.subject = subject;
    }

    public void doHobby(){
        hobby.doHobby();
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public void createHomework(String homework)
    {
         this.setCurrentHomework(homework);
         notifyObservers();
    }

    public void addObserver(Observer observer)
    {
        observers.add(observer);
    }
    public void removeObserver(Observer observer)
    {
        int toBeRemoved=observers.indexOf(observer);
        observers.remove(toBeRemoved);
    }

    public void notifyObservers()
    {
        for (Observer observer: observers)
        {
            observer.update();
        }
    }

    @Override
    public void update() {
        doAfterUpdate();
    }

    @Override
    public void doAfterUpdate() {
        System.out.println(name+" делает домашнюю работу с содержанием: "+observable.currentHomework());
    }

    @Override
    public String currentHomework() {
        return currentHomework;
    }
}
