package univer;

/**
 * Created by Ilya Evlampiev on 19.11.2018.
 */
public class TeacherCampus extends Teacher implements IHaveCampusRoom {
    public TeacherCampus(String name) {
        super(name);
    }

    @Override
    String whereLive() {
        return "Campus";
    }

    @Override
    public boolean fireSafetyChecking() {
        return true;
    }


}
