package univer;

/**
 * Created by Ilya Evlampiev on 26.11.2018.
 */
public class TeacherCreator extends PersonCreator {
    @Override
    public ISign create(String typeParameter, String name) {
        if (typeParameter.equals("0"))
        {
            return new TeacherCampus(name);
        }
        if (typeParameter.equals("1"))
        {
            return new TeacherFlat(name);
        }
        throw new RuntimeException("Wrong initialization");
    }
}
