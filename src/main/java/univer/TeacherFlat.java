package univer;

/**
 * Created by Ilya Evlampiev on 19.11.2018.
 */
public class TeacherFlat extends Teacher {
    public TeacherFlat(String name) {
        super(name);
    }

    @Override
    String whereLive() {
        return "Flat";
    }

}
