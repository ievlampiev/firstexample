package univer;

/**
 * Created by Ilya Evlampiev on 26.11.2018.
 */
public class TeacherSimpleFabric {

    Teacher createTeacher(String parameter, String name)
    {
        if (parameter.equals("0"))
        {
            return new TeacherCampus(name);
        }
        if (parameter.equals("1"))
        {
            return new TeacherFlat(name);
        }
        throw new RuntimeException("Wrong initialization");
    }

    Teacher createTeacher(TeacherType teacherType, String name)
    {
        if (teacherType==TeacherType.TEACHER_CAMPUS)
        {
            return new TeacherCampus(name);
        }
        if (teacherType==TeacherType.TEACHER_FLAT)
        {
            return new TeacherFlat(name);
        }
        throw new RuntimeException("Wrong initialization");
    }
}
