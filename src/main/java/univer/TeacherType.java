package univer;

/**
 * Created by Ilya Evlampiev on 26.11.2018.
 */
public enum TeacherType {
    TEACHER_FLAT,
    TEACHER_CAMPUS
}
