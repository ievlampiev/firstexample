package univer;

import univer.canteen.Ketchup;
import univer.canteen.Soup;
import univer.canteen.SourCream;
import univer.canteen.Spaghetti;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ilya Evlampiev on 19.11.2018.
 */
public class University {

    final int PREMIAL=1700;
    final int MONTHLY_PAYMENT= 2000;

    String rectorSurname="Гафуров";

    public static void main(String[] args) {
        University university = new University();
        //TeacherSimpleFabric teacherSimpleFabric=new TeacherSimpleFabric();
        //Teacher x=teacherSimpleFabric.createTeacher(TeacherType.TEACHER_CAMPUS,"x");
        //Teacher y=teacherSimpleFabric.createTeacher(TeacherType.TEACHER_FLAT,"y");

        PhilosophyTeacherFabric philosophyTeacherFabric=new PhilosophyTeacherFabric();
        ITISTeacherFabric itisTeacherFabric=new ITISTeacherFabric();
        Teacher x=philosophyTeacherFabric.createTeacher("0","x");
        Teacher y=itisTeacherFabric.createTeacher("1","y");

//        TeacherCreator teacherCreator=new TeacherCreator();
//        Teacher x=(Teacher)teacherCreator.createObjectAndLogSign("0","x");
//        x.setSubject(new Programming());
//        Teacher y=(Teacher)teacherCreator.createObjectAndLogSign("1","y");
//        y.setSubject(new Philosophy());
//        //TeacherCampus x=new TeacherCampus("x");
        //TeacherFlat y=new TeacherFlat("y");
        Teacher[] teachers =new Teacher[2];
        teachers[0]=x;
        teachers[1]=y;

        StudentCreator studentCreator= new StudentCreator();
        Student a= (Student) studentCreator.createObjectAndLogSign(" ","a");
        Student c= (Student) studentCreator.createObjectAndLogSign(" ","c");
        Student b= (Student) studentCreator.createObjectAndLogSign(" ","b");
      //  Student a=new Student("a",x);
        //Student b=new Student("b",x);
        //Student c=new Student("c",y);

        x.addObserver(a);
        a.setObservable(x);
        x.addObserver(b);
        b.setObservable(x);

        y.addObserver(c);
        c.setObservable(y);
        x.addObserver(y);
        y.setObservable(x);
        //y.addObserver(a);
        //y.addObserver(b);



        university.teach(teachers);
//        y.setSubject(new Programming());
//        y.setHobby(new Fishing());
//        x.setHobby(new Chorus());
        university.hobby(teachers);

//        university.teach(teachers);
        //new Student[3];
        //students[0]=a;
        //students[1]=b;
        //students[2]=c;
        IHaveAccess[] accesses=new IHaveAccess[3];
        accesses[0]=x;
        accesses[1]=a;
        accesses[2]=b;

        IHaveCampusRoom[] campusRoomers=new IHaveCampusRoom[3];
        campusRoomers[0]=(IHaveCampusRoom)x;
        campusRoomers[1]=c;
        campusRoomers[2]=b;

        university.giveKey(accesses);
        university.fireCheckingInCampus(campusRoomers);
        university.checkHouseTeacher(teachers);

        IGetPaid[] paid=new IGetPaid[2];
        paid[0]=x;
        paid[1]=y;

        x.createHomework("Homework 1 of teacher x");

        university.giveMoney(paid);
        //Teacher[] teachers1 = new Teacher[];
        //university.classWork(x,students);
        //university.homeWork(y,students);
        Student[] students1=university.classWork(x);
        //Student[] students2=university.homeWork(y);

        university.writeStudents(new FileUtil(),students1
        );
        university.showTotalGrade(students1);


        university.writeStudents(new FileUtil(),students1        );

        university.showTotalGrade(students1);

        y.createHomework("Homework 1 of teacher y");
        x.createHomework("Homework 2 of teacher x");
        x.removeObserver(b);
        x.createHomework("Homework 3 of teacher x");

        university.canteenLunch(students1);

        Room<Teacher,Student> room1=new Room<>();
        room1.setNumber(1);
        room1.setLiver(y);
        room1.setLiver2(a);
        //room1.setLiver(y);

        Room<Student,Student> room2=new Room<>();
        room2.setNumber(2);
        room2.setLiver(b);
        room2.setLiver2(c);

        //Room<Teacher,Teacher> room3=new Room<>();
        //room3.setNumber(3);
        //IHaveCampusRoom x1=(TeacherCampus)x;
        //IGetPaid x1=(TeacherCampus)x;
        TeacherCampus x1=(TeacherCampus)x;
        //room3.setLiver(x1);
        //room3.setLiver2(y);
        //room3.setLiver(x1);
        //room3.setLiver(x1);
        //room3.setLiver(a);
        //university.checkRooms(room3);
        //university.checkRooms(room2);
        university.checkRooms(room1);

        university.safetyRoolesSign(a);
        university.safetyRoolesSign(b);
        university.safetyRoolesSign(c);
        university.safetyRoolesSign(x);
        university.safetyRoolesSign(y);


        Director director=new Director();
        director.setName(university.rectorSurname);
        university.safetyRoolesSign(director);

        university.rectorSurname=" "+university.rectorSurname;

        university.safetyRoolesSign(new ISign() {
            @Override
            public String getName() {
                return "Зам "+university.rectorSurname;
            }

            @Override
            public void setName(String name) {

            }
        });
    }


    public void safetyRoolesSign(ISign iSign)
    {
        System.out.println("Подписываем ознакомление с техникой безопасности "+iSign.getSign());
    }

    public void checkRooms(Room<? extends Teacher,?> room)
    {
        System.out.println("Number of room is "+room.getNumber());
    }


    public void giveKey(IHaveAccess[] iHaveAccesses) {
        System.out.println("Начинается выдача ключа всем");
        for (IHaveAccess iHaveAccess : iHaveAccesses) {
            System.out.println("Выдается ключ человеку:");
            iHaveAccess.takeKey();
            System.out.println("Взявший ключ ставит роспись в журнале " + iHaveAccess.getSign());
        }
    }

    public void giveMoney(IGetPaid[] iGetPaids) {
        System.out.println("Начинается выдача денег всем");
        for (IGetPaid iGetPaid : iGetPaids) {
            System.out.println("Выдаются деньги человеку:");
            iGetPaid.takeMoney(PREMIAL);
            System.out.println("Получивший деньни человек расписывается "+ iGetPaid.getSign());
        }
    }

    public void receivePayment(IPay[] iPays)
    {
        System.out.println("Начинается прием денег у всех");
        for (IPay iPay: iPays)
        {
            System.out.println("Принимается платеж от человека: ");
            iPay.payMoney(MONTHLY_PAYMENT);
            System.out.println("Получивший деньни человек расписывается "+ iPay.getSign());
        }
    }

    public void fireCheckingInCampus(IHaveCampusRoom[] iHaveCampusRooms)
    {
        System.out.println("Начинается пожарная проверка у всех");
        for (IHaveCampusRoom iHaveCampusRoom: iHaveCampusRooms)
        {
            System.out.println("Проверка комнаты человека в общежитии прошла успешно (да/нет): "+iHaveCampusRoom.fireSafetyChecking());
            System.out.println("Получивший деньни человек расписывается " + iHaveCampusRoom.getSign());


        }
    }

    public void checkHouseTeacher(Teacher[] teachers){
        for (Teacher teacher : teachers){
            System.out.println("Анкетирование учителей выявило, что " + teacher.getSign() + " живет в " + teacher.whereLive() );
        }

    }

    public Student[] classWork(Teacher teacher)
    {
        FileUtil fileUtil=new FileUtil();
        Student[] students= new Student[0];
        try {
            students = fileUtil.readStudentsFromFile("students.txt");
        }
        catch (NotUniqueNameException e) {
            System.out.println(e.getMessage());
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        for (Student student: students)
        {
            student.setGrade(4);
            System.out.println("Преподаватель выставляет оценку за классную работу 4 "+teacher.getSign()+" студент "+student.getSign());
        };
         return students;
    }

    public Student[] homeWork(Teacher teacher)
    {
        FileUtil fileUtil=new FileUtil();
        Student[] students= new Student[0];
        try {
            students = fileUtil.readStudentsFromFile("students.txt");
        }
        catch (NotUniqueNameException e) {
            System.out.println(e.getMessage());
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        for (Student student: students)
        {
            student.setGrade(5);
            System.out.println("Преподаватель выставляет оценку за домашнюю работу 5 "+teacher.getSign()+" студент "+student.getSign());
        };
        return students;
    }

    public void writeStudents(FileUtil fileUtil, Student[] students)
    {

        try {
            fileUtil.writeStudentsToFile("students.txt", students);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void showTotalGrade(Student[] students)
    {
        for (Student student: students)
        {
            System.out.println("Общий балл студента "+student.getTotalGrade()+ " "+student.getSign());
        }
    }

    public void teach(Teacher[] teachers)
    {
        for (Teacher teacher: teachers)
        {
            System.out.println("Преподаватель проводит пару "+teacher.getSign());
            teacher.teach();
        }
    }
    public void hobby(Teacher[] teachers)
    {
        for (Teacher teacher: teachers)
        {
            System.out.println("Преподаватель проводит свободноу время "+teacher.getSign());
            teacher.doHobby();
        }
    }

    public void canteenLunch(Student[] students)
    {
        for (Student student: students)
        {
            student.eatLunch(new SourCream(new Soup()));
            student.eatLunch(new Ketchup(new Ketchup(new Spaghetti())));
        }
    }

}
