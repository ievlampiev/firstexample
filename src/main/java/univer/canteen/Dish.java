package univer.canteen;

/**
 * Created by Ilya Evlampiev on 10.12.2018.
 */
public abstract class Dish {

    public abstract String getName();

    public abstract int getCost();

}
