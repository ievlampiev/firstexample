package univer.canteen;

/**
 * Created by Ilya Evlampiev on 10.12.2018.
 */
public class Ketchup extends Toping {

    public Ketchup(Dish dish)
    {
        super(dish);
    }

    @Override
    public String getTopingName() {
        return " с кетчупом";
    }

    @Override
    public int getTopingCost() {
        return 5;
    }

}
