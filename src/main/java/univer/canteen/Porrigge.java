package univer.canteen;

/**
 * Created by Ilya Evlampiev on 10.12.2018.
 */
public class Porrigge extends Dish {
    @Override
    public String getName() {
        return "Каша";
    }

    @Override
    public int getCost() {
        return 25;
    }

}
