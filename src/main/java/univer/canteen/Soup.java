package univer.canteen;

/**
 * Created by Ilya Evlampiev on 10.12.2018.
 */
public class Soup extends Dish {
    @Override
    public String getName() {
        return "Суп";
    }

    @Override
    public int getCost() {
        return 30;
    }

}
