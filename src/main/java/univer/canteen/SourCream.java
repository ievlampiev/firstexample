package univer.canteen;

/**
 * Created by Ilya Evlampiev on 10.12.2018.
 */
public class SourCream extends Toping {

    public SourCream(Dish dish) {
        super(dish);
    }

    @Override
    public String getTopingName() {
        return " со сметаной";
    }

    @Override
    public int getTopingCost() {
        return 5;
    }
}
