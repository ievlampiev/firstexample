package univer.canteen;

/**
 * Created by Ilya Evlampiev on 10.12.2018.
 */
public class Spaghetti extends Dish {
    @Override
    public String getName() {
        return "Макароны";
    }

    @Override
    public int getCost() {
        return 15;
    }

}
