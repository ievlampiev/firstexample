package univer.canteen;

/**
 * Created by Ilya Evlampiev on 10.12.2018.
 */
public abstract class Toping extends Dish{
    protected Dish dish;

    public Toping(Dish dish){
        this.dish=dish;
    }

    public String getName(){
       return dish.getName()+getTopingName();
   }
    public int getCost(){
        return dish.getCost()+getTopingCost();
    }
    public abstract String getTopingName();

    public abstract int getTopingCost();

    public Dish getDish() {
        return dish;
    }
}
