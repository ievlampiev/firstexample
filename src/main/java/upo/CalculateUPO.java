package upo;

/**
 * Created by Ilya Evlampiev on 29.10.2018.
 */
public interface CalculateUPO {
    /**
     *
     * @param yacht Принимается объект {@link upo.Yacht} в качестве DTO
       @param yacht1 Принимается (на самом деле нет) еще какой-то другой объект
     * @return Возвращается длинна (вернется эта строчка)
     * @return Возвращается длинна (не вернется эта строчка)
     * @see upo.KnukledYacht#ohat
     * @see upo.KnukledYacht#getOhat()
     * @since 1.1
     */
    double measuringLength(Yacht yacht);
}
