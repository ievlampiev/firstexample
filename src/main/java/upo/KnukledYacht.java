package upo;

/**
 * Created by Ilya Evlampiev on 29.10.2018.
 */
public class KnukledYacht extends Yacht {
    /**
     * горизонтальное расстояние от заднего конца ватерлинии до
     нижней точки транца
     */
    public double ohat;
    private double ha;
    private double agoRevised;

    /**
     * горизонтальное расстояние от заднего конца ватерлинии до
     нижней точки транца
     * @return
     */
    public double getOhat() {
        return ohat;
    }

    /**
     * горизонтальное расстояние от заднего конца ватерлинии до
     нижней точки транца
     from outside - это попадет в джавадок
     * @param ohat
     */

    public void setOhat(double ohat) {
        /**
         * from inside - а это нет
         */
        this.ohat = ohat;
    }

    public double getHa() {
        return ha;
    }

    public void setHa(double ha) {
        this.ha = ha;
    }

    public double getAgoRevised() {
        return agoRevised;
    }

    public void setAgoRevised(double agoRevised) {
        this.agoRevised = agoRevised;
    }
}
