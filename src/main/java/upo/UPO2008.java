package upo;

/**
 * Created by Ilya Evlampiev on 29.10.2018.
 * <br>
 * Класс, который выполняет подсчет длинны яхты получив на вход объект яхты
 * <br>
 * В классе реализуется расчет по правилам <a href="https://vk.com/doc409204948_478522504?hash=5d8fb5de47b7a1a009&dl=d03da821d2c4fdc248">UPO2008</a>, расширяет интерфейс {@link upo.CalculateUPO}, который нужен для общего подхода, вне зависимости от версии алгоритма
 *
 * @author Ilya Evlampiev
 * @version 1.0-SNAPSHOT
 *
 *
 */
public class UPO2008 implements CalculateUPO {
    private double l;
    private double bwl;
    private double g;
    private double ago;

    /**{@inheritDoc}
     * а сюда дополним
     */
    @Override
    /**
     * дополним второй раз и отсюда ничего не попадет в джавадок
     */
    public double measuringLength(Yacht yacht) {
        bwl = yacht.getSbmax() - yacht.getBmax();
        g = yacht.getGmax1() - yacht.getFg1() - yacht.getFg2();

        ago=yacht.getAgo();

        if (yacht instanceof KnukledYacht)
        {
            ago=((KnukledYacht)yacht).getOhat() *
                    (1 - 0.03 * (bwl + g) / ((KnukledYacht)yacht).getHa());
            ((KnukledYacht)yacht).setAgoRevised(ago);
        }

        return l = yacht.getLoa() - yacht.getFgo() - ago;
    }
}
