package upo;

/**
 * Created by Ilya Evlampiev on 29.10.2018.
 */
public class Yacht {
    private double loa;
    private double fgo;
    private double ago;
    private double sbmax;
    private double bmax;
    private double gmax1;
    private double fg1;
    private double fg2;

    public double getLoa() {
        return loa ;
    }

    public void setLoa(double loa) {
        this.loa = loa;
    }

    public double getFgo() {
        return fgo;
    }

    public void setFgo(double fgo) {
        this.fgo = fgo;
    }

    public double getAgo() {
        return ago;
    }

    public void setAgo(double ago) {
        this.ago = ago;
    }

    public double getSbmax() {
        return sbmax;
    }

    public void setSbmax(double sbmax) {
        this.sbmax = sbmax;
    }

    public double getBmax() {
        return bmax;
    }

    public void setBmax(double bmax) {
        this.bmax = bmax;
    }

    public double getGmax1() {
        return gmax1;
    }

    public void setGmax1(double gmax1) {
        this.gmax1 = gmax1;
    }

    public double getFg1() {
        return fg1;
    }

    public void setFg1(double fg1) {
        this.fg1 = fg1;
    }

    public double getFg2() {
        return fg2;
    }

    public void setFg2(double fg2) {
        this.fg2 = fg2;
    }
}
