package upo;

import org.junit.Test;
import univer.Teacher;
import univer.TeacherCampus;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
/**
 * Created by Ilya Evlampiev on 03.12.2018.
 */
public class UpoTest {


    @Test
    public void testL()
    {
       Yacht yacht = new Yacht();
       UPO2008 calculator = new UPO2008();
       yacht.setSbmax(4.2);
       yacht.setBmax(-0.04);
       yacht.setFg1(0.8);
       yacht.setFg2(0.8);
       yacht.setFgo(0.55);
       yacht.setGmax1(1);
       yacht.setLoa(6.9);

       assertEquals(calculator.measuringLength(yacht), 6.35, 0.001);


    }

    @Test
    public void testL2()
    {
        Teacher teacher=new TeacherCampus("Ivan");
        org.junit.Assert.assertNotNull(teacher);
        org.junit.Assert.assertEquals(teacher.getSign(), "(подпись учителя) Ivan");
        //assertTrue(false);
    }
}
